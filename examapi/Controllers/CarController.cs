﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace examapi.Controllers
{

    public class Car
    {

        public string mf { get; set; }
        public string color { get; set; }
        public string model { get; set; }

  
    }
    public class CarController : Controller
    {
        // GET: Car
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult list(string id)
        {
             if (! authenticate(id))
            {

                throw new UnauthorizedAccessException();
            }

            List<Car> listOfCars = getCarCollection();

            return Json(listOfCars, JsonRequestBehavior.AllowGet);
        }


        public bool authenticate(string id)
        {

            List<String> apiKeys = new List<String> { "a123", "b456", "c789" };

            return apiKeys.Contains(id);
        }
        public List<Car> getCarCollection()
        {

            List<Car> listOfCars = new List<Car>();

            listOfCars.Add(new Car { color = "Red", mf = "Ford", model = "Kuga" });
            listOfCars.Add(new Car { color = "Red", mf = "Skoda", model = "Octavia" });
            listOfCars.Add(new Car { color = "Red", mf = "Nissan", model = "Navara" });

            return listOfCars;
        }
    }
}