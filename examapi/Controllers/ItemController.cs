﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace examapi.code
{

    public class Item
    {

        public string Title { get; set; }

    }


    public class ItemController : Controller
    {
        // GET: Item
        public ActionResult Index()
        {
            return View();
        }

        // GET: Item
        public JsonResult list(string id)
        {

            if (! validateApiKey(id))
            {
                throw new UnauthorizedAccessException();

            }
            // return Json(new { Title = "Tomatoes" },JsonRequestBehavior.AllowGet);

            // return Json(listOfItems, JsonRequestBehavior.AllowGet);


          //  List<Item> retrieveItems = retrieveList();
          //  return Json(retrieveItems, JsonRequestBehavior.AllowGet);

           
           return Json(retrieveListOfItems(), JsonRequestBehavior.AllowGet);


        }

        public List<Item> retrieveListOfItems()
        {

            Item newItem1 = new Item { Title = "Tomatoes" };
            Item newItem2 = new Item { Title = "Potatoes" };
            Item newItem3 = new Item { Title = "Carrots" };

            List<Item> listOfItems = new List<Item>();
            listOfItems.Add(newItem1);
            listOfItems.Add(newItem2);
            listOfItems.Add(newItem3);

            return listOfItems;
        }

        public bool validateApiKey(string id)
        {

            List<String> apiKeys = new List<String> { "123", "456", "789" };

            return apiKeys.Contains(id);

        }
    }
}